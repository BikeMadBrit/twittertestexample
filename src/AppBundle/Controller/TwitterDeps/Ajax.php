<?php

namespace AppBundle\Controller\TwitterDeps;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Classes\TwitterAPIExchange;

trait Ajax
{
    /**
     * @Route(
     *    "/api/twitter-feed/{lat}/{lng}/{search}/{count}/{distance}", 
     *     defaults={"count"="50", "distance"="50"},
     *     options={"expose"=true}, 
     *     name="get_twitter_feed"
     * )
     * @Method({"GET", "POST"})
     */
    public function getTwitterFeedApiAction(Request $request, $lat, $lng, $search, $count = 50, $distance = 50)
    {
        $url = 'https://api.twitter.com/1.1/search/tweets.json';
        $requestMethod = 'GET';

        try 
        {
            $twitter = new TwitterAPIExchange([
                'consumer_key'              => 'fM9iRFVDmK8oBQLTCTWRLbED7',
                'consumer_secret'           => 'kSNTm38YHXeXIpuslzvyoiHxV0XLwH4Ip03bftHdEC5gSqxQXi',
                'oauth_access_token'        => '911868125989048321-guJQHqtihynneOX5YcN721e18oB0vzp',
                'oauth_access_token_secret' => '1uOOZIB8bRgZ54Tng4W0w5h8ZcbIyHOq1cOfvPxkLrLSg'
            ]);

            $get = "?geocode={$lat},{$lng},{$distance}km&count={$count}&result_type=recent&q={$search}";

            $response = $twitter->setGetfield($get)
                ->buildOauth($url, 'GET')
                ->performRequest();

            $tweets = $this->generateFeed($response);

        } 
        catch (Exception $e) 
        {
            return new Response(json_encode(['errors' => $e->getMessage()]), 500);
        }

        if (isset($tweets['errors'])) new Response(json_encode([$tweets]), 403);

        return new Response(json_encode($tweets), 200);
    }
}

<?php

namespace AppBundle\Controller\TwitterDeps;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait Routes
{
	/**
	 * @Route("/")
	 */
	public function indexAction()
	{
		return $this->render('twitter/index.html', []);
	}
}

<?php

namespace AppBundle\Controller\TwitterDeps;

trait Methods
{
	private function generateFeed($response) 
	{
		$entries = '';
		$maxTime = 0;

		$tweets = json_decode($response, 1);
		$tweets = $tweets['statuses'];

		$tweetsOut = [];

		if (isset($tweets['errors'])) 
		{
			return ['errors' => $tweets['errors']];
		} 
		else 
		{
			foreach($tweets as $tweet) 
			{
				if (!$coordinates = $this->getLocation($tweet)) continue;
				if (empty($tweet['user']['profile_image_url'])) continue;

				$id   = $tweet['id_str'];
				$user = $tweet['user']['screen_name'];
				$time = strtotime($tweet['created_at']);

				$tweetsOut[] = [
					'id'     => $id,
					'user'   => $user,
					'avatar' => $tweet['user']['profile_image_url'],
					'link'   => "https://twitter.com/$user/status/{$id}",
					'text'   => $tweet['text'],
					'time'   => strtotime($tweet['created_at']),
					'date'   => date('H:m d/m/Y', $time),
					'lat'    => $coordinates['lat'],
					'lng'    => $coordinates['lng']
				];
			}
		}

		return $tweetsOut;
	}

	private function getLocation($tweet)
	{
		if (empty($tweet['geo']) || empty($tweet['coordinates'])) return false;

		$lat = -1;
		$lng = -1;

		if (!empty($tweet['geo']))
		{
			$lat = $tweet['geo']['coordinates'][0];
			$lng = $tweet['geo']['coordinates'][1];
		}
		else if (!empty($tweet['coordinates']))
		{
			$lat = $tweet['coordinates']['coordinates'][1];
			$lng = $tweet['coordinates']['coordinates'][0];
		}

		return ['lat' => $lat, 'lng' => $lng];
	}
}

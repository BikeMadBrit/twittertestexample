<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller; 
use AppBundle\Controller\TwitterDeps as Deps;

class TwitterController extends Controller
{
	use Deps\Properties;
	use Deps\Methods;
	use Deps\Routes;
	use Deps\Ajax;
}
